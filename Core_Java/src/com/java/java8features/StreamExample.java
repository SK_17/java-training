package com.java.java8features;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class StreamExample {


	public static void main(String[] args) {
		
		List<EmployeeDetails> list = new ArrayList<EmployeeDetails>();
		list.add(new EmployeeDetails(1, "Sakshi", "Intern", 25000));
		list.add(new EmployeeDetails(2, "Shivani", "Manager", 35000));
		list.add(new EmployeeDetails(3, "Shreya", "Senior Manager", 65000));
		list.add(new EmployeeDetails(4, "Fiona", "Manager", 35000));
		list.add(new EmployeeDetails(5, "Priya", "Intern", 25000));
		list.add(new EmployeeDetails(6, "Riya", "Manager", 35000));
		
		System.out.println(list);
		
		Set<String> s = new HashSet<>();
		for(EmployeeDetails eds : list) {
			s.add(eds.getDesignation());
		}
		
		for(String val : s) {
			System.out.println("val : "+val);
		}
		
		list.stream()
            .map(EmployeeDetails::getDesignation )
            .distinct()
            .forEach(System.out::println);
		
	}

}
