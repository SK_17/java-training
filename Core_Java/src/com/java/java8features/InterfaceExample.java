package com.java.java8features;

public class InterfaceExample {

	public static void main(String[] args) {
		
		Details detail =(msg)->
		System.out.println("The details are id =" +msg.getId()+" Name = "+msg.name+" Designation = "+msg.getDesignation()+" Salary = "+msg.salary);
		
		EmployeeDetails emp1 =new EmployeeDetails(1, "Sakshi", "Intern", 30000);
		EmployeeDetails emp2 =new EmployeeDetails(2, "Shivani", "Manager", 35000);
		EmployeeDetails emp3 =new EmployeeDetails(3, "Shreya", "Senior Manager", 65000);
		EmployeeDetails emp4 =new EmployeeDetails(4, "Fiona", "Manager", 35000);
		
		detail.display(emp1);
		detail.display(emp2);
		detail.display(emp3);
		detail.display(emp4);
	}

}
