package com.java.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ArraylistEmployee {

	public static void main(String[] args) {

	
	ArrayList<Employee> emp = new ArrayList<Employee>();
	
	System.out.println("Sorted by location");

	
	emp.add(new Employee("Sakshi", "Seawoods"));
	emp.add(new Employee("Priya", "Marol Naka"));
	emp.add(new Employee("Shivani", "Airpod Road"));
	emp.add(new Employee("Shreya", "Marol Naka"));
	emp.add(new Employee("Radha", "Seawoods"));
	emp.add(new Employee("Fiona", "Seawoods"));
	emp.add(new Employee("Priyanka", "Marol Naka"));
	
	Collections.sort(emp, new LocationComparator());
	
	for(Employee e:emp) {
		System.out.println("Name ="+ e.name+ " "+"Location ="+e.location);
	}

	}}
