package com.java.comparator;

import java.util.Collections;
import java.util.LinkedList;

public class LinklistEmployee {

	public static void main(String[] args) {
		
		LinkedList<Employee> emp = new LinkedList<Employee>();
		
		emp.add(new Employee("Radha", "Seawoods"));
		emp.add(new Employee("Fiona", "Vashi"));
		emp.add(new Employee("Sayali", "Panvel"));
		emp.add(new Employee("Calvin", "Seawoods"));
		emp.add(new Employee("Shubham", "Vashi"));
		
		Collections.sort(emp , new LocationComparator());
		for(Employee e:emp) {
			System.out.println(e.name +" "+e.location);
		}

	}

}
