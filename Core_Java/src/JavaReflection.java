import java.lang.reflect.Field;

public class JavaReflection {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {


		//phone
		
		// iphone, samsung
		
		//phone [camera, editing]
		
		//Student s = new Student();
		
		Class c = Class.forName("StudentEx");
		System.out.println(c.getName());
		
		StudentEx s = (StudentEx) c.newInstance();
		s.fname();
		s.lname(); 
		
		Field f1 = s.getClass().getField("name");
		
		f1.set(s,"Sakshi Khalate");
		String Name = (String) f1.get(s);
		System.out.println("Name "+ Name);
	}

}
